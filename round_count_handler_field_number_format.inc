<?php

/**
 * @file
 * Definition of round_count_handler_field_number_format.
 */

/**
 * Render a field as a round thousands.
 *
 * Definition terms:
 * - float: If true this field contains a decimal value. If unset this field
 *          will be assumed to be integer.
 *
 * @ingroup views_field_handlers
 */
class round_count_handler_field_number_format extends views_handler_field_numeric {

  /**
   * Extend option_definition().
   */
  public function option_definition() {
    $options = parent::option_definition();

    $options['round_number'] = array('default' => FALSE, 'bool' => TRUE);

    return $options;
  }

  /**
   * Extend options_form().
   */
  public function options_form(&$form, &$form_state) {

    $form['round_number'] = array(
      '#type' => 'checkbox',
      '#title' => t('Count round'),
      '#description' => t('Number round thousands to a K style.'),
      '#default_value' => $this->options['round_number'],
    );
    parent::options_form($form, $form_state);
  }

  /**
   * Extend render().
   */
  public function render($values) {
    $value = $this->get_value($values);

    // Should we format as a round thousands.
    if (!empty($this->options['round_number'])) {
      $value = round_thousands($value);
    }

    return $this->sanitize_value($this->options['prefix'], 'xss')
      . $this->sanitize_value($value)
      . $this->sanitize_value($this->options['suffix'], 'xss');
  }

}

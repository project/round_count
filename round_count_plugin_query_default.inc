<?php

/**
 * @file
 * Defines the default query object.
 */

/**
 * Object used to create a SELECT query.
 *
 * @ingroup views_query_plugins
 */
class round_count_plugin_query_default extends views_plugin_query_default {

  /**
   * Override get_aggregation_info()
   */
  public function get_aggregation_info() {
    // @todo -- need a way to get database specific and customized aggregation
    // functions into here.
    return array(
      'group' => array(
        'title' => t('Group results together'),
        'is aggregate' => FALSE,
      ),
      'count' => array(
        'title' => t('Count'),
        'method' => 'views_query_default_aggregation_method_simple',
        'handler' => array(
          'argument' => 'views_handler_argument_group_by_numeric',
          'field' => 'round_count_handler_field_number_format',
          'filter' => 'views_handler_filter_group_by_numeric',
          'sort' => 'views_handler_sort_group_by_numeric',
        ),
      ),
      'count_distinct' => array(
        'title' => t('Count DISTINCT'),
        'method' => 'views_query_default_aggregation_method_distinct',
        'handler' => array(
          'argument' => 'views_handler_argument_group_by_numeric',
          'field' => 'round_count_handler_field_number_format',
          'filter' => 'views_handler_filter_group_by_numeric',
          'sort' => 'views_handler_sort_group_by_numeric',
        ),
      ),
      'sum' => array(
        'title' => t('Sum'),
        'method' => 'views_query_default_aggregation_method_simple',
        'handler' => array(
          'argument' => 'views_handler_argument_group_by_numeric',
          'filter' => 'views_handler_filter_group_by_numeric',
          'sort' => 'views_handler_sort_group_by_numeric',
        ),
      ),
      'avg' => array(
        'title' => t('Average'),
        'method' => 'views_query_default_aggregation_method_simple',
        'handler' => array(
          'argument' => 'views_handler_argument_group_by_numeric',
          'filter' => 'views_handler_filter_group_by_numeric',
          'sort' => 'views_handler_sort_group_by_numeric',
        ),
      ),
      'min' => array(
        'title' => t('Minimum'),
        'method' => 'views_query_default_aggregation_method_simple',
        'handler' => array(
          'argument' => 'views_handler_argument_group_by_numeric',
          'filter' => 'views_handler_filter_group_by_numeric',
          'sort' => 'views_handler_sort_group_by_numeric',
        ),
      ),
      'max' => array(
        'title' => t('Maximum'),
        'method' => 'views_query_default_aggregation_method_simple',
        'handler' => array(
          'argument' => 'views_handler_argument_group_by_numeric',
          'filter' => 'views_handler_filter_group_by_numeric',
          'sort' => 'views_handler_sort_group_by_numeric',
        ),
      ),
      'stddev_pop' => array(
        'title' => t('Standard deviation'),
        'method' => 'views_query_default_aggregation_method_simple',
        'handler' => array(
          'argument' => 'views_handler_argument_group_by_numeric',
          'filter' => 'views_handler_filter_group_by_numeric',
          'sort' => 'views_handler_sort_group_by_numeric',
        ),
      ),
    ) + views_fetch_plugin_data('query_aggregate');
  }

}

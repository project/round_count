<?php

/**
 * @file
 */

/**
 * Implements hook_views_data_alter().
 */
function round_count_views_data_alter(&$data) {
  foreach ($data as $module => &$table) {
    foreach ($table as $id => &$field) {
      if (isset($field['field']['handler']) && $field['field']['handler'] == 'views_handler_field_numeric') {
        $field['field']['handler'] = 'round_count_handler_field_number_format';
      }
    }
  }
}

/**
 * Implements hook_views_plugins_alter().
 */
function round_count_views_plugins_alter(&$plugins) {
  $plugins['query']['views_query']['handler'] = 'round_count_plugin_query_default';
}
